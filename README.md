# CSD Project

Use this gist to get started with a CSD project.

## Getting started

To get started, install the required node modules:

```
npm install
```

Then issue the following command to run the server:

```
npm run start
```

To run test Suite

```
npm run test
```

Navigate to http://localhost:3001/ and files from the public folder will start being served.
