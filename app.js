const express = require('express');
const savePerson = require('./modules/savePerson');
const updateMentor = require('./modules/updateMentor');
const getList = require('./modules/getList');
const saveGoals = require('./modules/saveGoals');
const login = require('./modules/login');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
app.use(cookieParser());

// Sample JSON structure

// data = [{
//   id: '#',
//   mentor: {
//     name: "",
//   },
//   mentee: {
//     name: "",
//   }
// },{
//   id: '#',
//   mentor: {
//     name: "",
//   },
//   mentee: {
//     name: "",
//   }
// }];

let data = {};

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', function (request, response) {
    response.sendFile(__dirname + '/public/index.html');
});

app.post('/register', function (request, response, next) {
    data = {};
    try {
        data = savePerson(
            request.body.firstName,
            request.body.lastName,
            request.body.email,
            request.body.password,
            request.body.role
        );
    } catch (e) {
        data.error = e;
    }
    var cookie = request.cookies.userDetails;
    if (cookie === undefined && !data.error) {
        // no: set a new cookie
        if (data.Password) {
            delete data.Password;
        }
        response.cookie('userDetails', JSON.stringify(data), {
            maxAge: 900000,
        });
    }
    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify(data));
});

app.post('/login', function (request, response, next) {
    data = {};
    try {
        data = login(request.body.email, request.body.password);
    } catch (e) {
        data.error = e;
    }
    var cookie = request.cookies.userDetails;
    if (cookie === undefined && !data.error) {
        // no: set a new cookie
        if (data.Password) {
            delete data.Password;
        }
        response.cookie('userDetails', JSON.stringify(data), {
            maxAge: 900000,
        });
    }
    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify(data));
});

app.post('/updateMentor', function (request, response, next) {
    data = {};
    try {
        data = updateMentor(request.body.mentorEmail, request.body.menteeEmail);
    } catch (e) {
        data.error = e;
    }
    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify(data));
});

app.post('/getList', function (request, response, next) {
    data = {};
    try {
        data = getList(request.body.listRole);
    } catch (e) {
        // exception thrown
        data.error = e;
    }
    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify(data));
});

app.post('/add-goals', function (request, response, next) {
    data = {};
    try {
        data = saveGoals(
            request.body.goals,
            request.body.timeframe,
            request.body.email
        );
    } catch (e) {
        // exception thrown
        data.error = e;
    }

    response.cookie('userDetails', JSON.stringify(data), {
        maxAge: 900000,
    });

    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify(data));
});

app.listen(3001, function () {
    console.log('Server listening on port 3001.');
});
