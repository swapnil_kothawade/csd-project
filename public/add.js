const genericError = "An error occurred. Please contact your administrator.";
const columns =  ["FirstName", "LastName", "Email", "MenteeEmail"];
const container = document.getElementById("mentorList");
function addMenteeToMentor(mentorEmail){
	var menteeEmail = document.getElementById(mentorEmail).value
	console.log(mentorEmail);
	console.log(menteeEmail);
			
	if(!mentorEmail || !menteeEmail){
		$("#warning-box").html(`<div class="alert alert-danger" role="alert">${genericError}</div>`);
	}
	const postData = {"mentorEmail":mentorEmail,"menteeEmail":menteeEmail};
	$.post("/updateMentor", postData, function(data){
		if(!data){
			data = { error : genericError};
		}
		if(data.error){
			$("#warning-box").html(`<div class="alert alert-danger" role="alert">${data.error}</div>`);
		} else {
			success('Mentee Successfully Assigned!');
		}
	}, 'json');
}
function success(msg){
	$("#warning-box").html(`<div class="alert alert-success" role="alert">${msg}</div>`);
	//listViewObj = new listView('person', 'Mentor', container, columns);
	//listViewObj.refreshList();
}