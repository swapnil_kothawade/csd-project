const genericError = 'An error occurred. Please contact your administrator.';
const columns = ['FirstName', 'LastName', 'Email', 'MenteeEmail'];
const container = document.getElementById('mentorList');

$(document).ready(function () {
    const genericError =
        'An error occurred. Please contact your administrator.';
    const columns = ['FirstName', 'LastName', 'Email', 'MenteeEmail'];
    const container = document.getElementById('mentorList');
    let listViewObj = new listView('Mentor', container, columns);
    let userDetails =
        getCookie('userDetails') &&
        JSON.parse(decodeURIComponent(getCookie('userDetails')));
    refreshList(userDetails);

    function arrToUl(root, arr) {
        var ul = document.createElement('ul');
        var li;

        root.appendChild(ul); // append the created ul to the root
        arr.forEach(function (item) {
            li = document.createElement('li'); // create a new list item
            li.appendChild(
                document.createTextNode(item.timeframe + ': ' + item.goal)
            ); // append the text to the li
            ul.appendChild(li); // append the list item to the ul
        });
    }

    if (
        userDetails &&
        userDetails.Role.length &&
        userDetails.Role.includes('Mentee')
    ) {
        $('#goal-container').show();
    } else {
        $('#goal-container').hide();
    }

    if (userDetails && userDetails.Email) {
        $('#login-button').hide();
        $('#register-button').hide();
    }

    console.log('userDetails', userDetails);
    $('#loginForm').hide();
    $('#registrationForm').hide();

    function refreshList(userDetails) {
        if (userDetails && userDetails.Email) {
            $('#loginForm').hide();
            $('#registrationForm').hide();
        }

        if (
            userDetails &&
            userDetails.Role.length &&
            userDetails.Role.includes('Mentee')
        ) {
            if (userDetails.Goals && userDetails.Goals.length > 0) {
                var div = document.getElementById('goalList');

                arrToUl(div, userDetails.Goals);
            }
        }
    }

    $('#loginForm').hide();
    $('#registrationForm').hide();
    listViewObj.refreshList();
    $('#registrationForm').submit(function (e) {
        e.preventDefault();
        const postData = $(this).find(':input').serialize();
        $.post(
            '/register',
            postData,
            function (data) {
                if (!data) {
                    data = { error: genericError };
                }
                if (data.error) {
                    err(data.error);
                } else {
                    userDetails =
                        getCookie('userDetails') &&
                        JSON.parse(
                            decodeURIComponent(getCookie('userDetails'))
                        );
                    $('#goal-container').show();
                    $('#login-button').hide();
                    $('#register-button').hide();
                    $('#loginForm').hide();
                    $('#registrationForm').hide();
                }
            },
            'json'
        );
        e.currentTarget.reset();
    });
    $('#loginForm').submit(function (e) {
        e.preventDefault();
        const postData = $(this).find(':input').serialize();
        $.post('/login', postData, function (data) {
            if (!data) {
                data = { error: genericError };
            }
            if (data.error) {
                err(data.error);
            } else {
                userDetails =
                    getCookie('userDetails') &&
                    JSON.parse(decodeURIComponent(getCookie('userDetails')));
                $('#goal-container').show();
                $('#login-button').hide();
                $('#register-button').hide();
                $('#loginForm').hide();
                $('#registrationForm').hide();
            }
        });
        location.reload();
    });

    $('.mentee-button').click(function () {
        var mentorEmail = $(this).mentorEmail;
        var menteeEmail = $('#' + mentorEmail).value;
        console.log(mentorEmail);
        console.log(menteeEmail);
        if (!mentorEmail || !menteeEmail) {
            err(genericError);
        }
        const postData = { mentorEmail: mentorEmail, menteeEmail: menteeEmail };
        $.post(
            '/updateMentor',
            postData,
            function (data) {
                if (!data) {
                    data = { error: genericError };
                }
                if (data.error) {
                    err(data.error);
                } else {
                    success('Mentee Successfully Assigned!');
                }
            },
            'json'
        );
    });

    function err(msg) {
        $('#warning-box').html(
            `<div class="alert alert-danger" role="alert">${msg}</div>`
        );
    }

    function success(msg) {
        $('#warning-box').html(
            `<div class="alert alert-success" role="alert">${msg}</div>`
        );
        listViewObj = new listView('Mentor', container, columns);
        listViewObj.refreshList();
    }

    function getCookie(cname) {
        var name = cname + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }

    $('#goalForm').submit(function (e) {
        e.preventDefault();
        if (
            userDetails.Role &&
            userDetails.Role.length &&
            userDetails.Role.includes('Mentee')
        ) {
            const postData = $(this).find(':input').serializeArray();
            const emailObj = {
                name: 'email',
                value: userDetails.Email,
            };
            postData.push(emailObj);
            $.post(
                '/add-goals',
                postData,
                function (data) {
                    if (!data) {
                        data = { error: genericError };
                    }
                    if (data.error) {
                        $('#warning-box').html(
                            `<div class="alert alert-danger" role="alert">${data.error}</div>`
                        );
                    }
                    console.log('added new goals!');
                    $('#goalList').empty();
                    refreshList(data);
                },
                'json'
            );
            console.log('postData', postData);
            e.currentTarget.reset();
        } else {
            err('You cannot add a goal as you are not a mentee');
        }
    });

    $('.js-login').on('click', function () {
        $('#registrationForm').hide();
        $('#loginForm').show();
    });

    $('.js-register').on('click', function () {
        $('#loginForm').hide();
        $('#registrationForm').show();
    });

    /*
$('.mentee-button').click(function(){
	var mentorEmail = $(this).mentorEmail;
	var menteeEmail = $("#" + mentorEmail).value;
	console.log(mentorEmail);
	console.log(menteeEmail);
	if(!mentorEmail || !menteeEmail){
		$("#warning-box").html(`<div class="alert alert-danger" role="alert">${genericError}</div>`);
	}
	const postData = {"mentorEmail":mentorEmail,"menteeEmail":menteeEmail};
	$.post("/updateMentor", postData, function(data){
		if(!data){
			data = { error : genericError};
		}
		if(data.error){
			$("#warning-box").html(`<div class="alert alert-danger" role="alert">${data.error}</div>`);
		} else {
			success('Mentee Successfully Assigned!');
		}
	}, 'json');
});
*/
});
