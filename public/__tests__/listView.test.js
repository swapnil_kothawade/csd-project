/**
 * @jest-environment jsdom
 */

//jest.mock('../js/listView.js');

test('should populate a list of mentors', () => {
	const testData = [];
	for(let i=0; i<5; i++){
		testData.push({
			FirstName : "Test",
			LastName : 'Mentor${i}',
			Email : `TestMentor${i}@example.com`,
			Mentee : `TestMentee${i}@example.com`
		});
	}
	document.body.innerHTML =
    '<table>' +
    '  <tbody id="testContainer" />' +
    '</table>';
	const listView = require('../js/listView');
	const columns =  [ "FirstName", "LastName", "Email", "Mentee Email"];
	const container = document.getElementById('testContainer');
	let listViewObj = new listView('Mentor', container, columns);
	listViewObj.handleCallback(testData);
	expect(container.querySelectorAll('tr').length).toBe(5);
	//expect(container.querySelectorAll('td').length).toBe(20);
});

test('should populate the postData object', () => {
	document.body.innerHTML =
    '<table>' +
    '  <tbody id="testContainer" />' +
    '</table>';
	const listView = require('../js/listView');
	const columns =  [ "FirstName", "LastName", "Email", "Mentee Email"];
	const container = document.getElementById('testContainer');
	let listViewObj = new listView('Mentor', container, columns);
	let result = listViewObj.postData;
	expect(result.listRole).toBe("Mentor");
})
