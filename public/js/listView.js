class listView {

	constructor(listRole, container, columns){
		this.listRole = listRole;
		this.container = container;
		this.columns = columns;
	}

	get postData(){
		return {
			listRole : this.listRole
		}
	}
	refreshList(){
		let instance = this;
		$.ajax({ context: instance, type: "POST", data: JSON.stringify(instance.postData), url: "/getList", success: instance.handleCallback, contentType : "application/json"});	
	}
	handleCallback(data){
		let innerHTML = "";
		let instance = this;
		if(data && !data.error && data.length){
			data.forEach((row)=>{
				let rowHTML = '<tr>';
				instance.columns.forEach((column)=>{
					if(row.hasOwnProperty(column)){
						rowHTML += `<td>${row[column]}</td>`;
					}else if(column == "MenteeEmail"){
						var disabled = (row.hasOwnProperty(column) && `${row[column]}`)?'disabled':'';
						var email = (row.hasOwnProperty('Email'))?`${row["Email"]}`:'';
						var menteeEmail = (row.hasOwnProperty(column))?`${row[column]}`:'';

						rowHTML += '<td><input id="';
						rowHTML += email;
						rowHTML += '" type="email" ';
						rowHTML += disabled;
						rowHTML += ' value="';
						rowHTML += menteeEmail;
						rowHTML += '"></input>';
						if(disabled == ''){
							rowHTML += '<button onclick="addMenteeToMentor(\'' + email + '\')"';
							rowHTML += ' class="btn btn-default mentee-button">Save</button></td>';
						}
					}
				});
				rowHTML += '</tr>';
				innerHTML += rowHTML;
			});
		}
		this.container.innerHTML = innerHTML;
	}
}

if(typeof module!==undefined){
	module.exports = listView;
}