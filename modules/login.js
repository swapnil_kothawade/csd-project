function login(email, password){
	"use strict";
	const fs = require("fs");
	const path = require("path");
	const exceptions = require("./exceptions");

	let rawData = fs.readFileSync(path.resolve(__dirname, "../json/person.json"));
	let people = JSON.parse(rawData);

	let returnValue;
	people.forEach((person) => {
		if (person.Email === email && person.Password === password) {
			returnValue = person;
		}
	});
	if(returnValue){
		return returnValue;
	} else {
		throw exceptions.INVALID_LOGIN;
	}
}
module.exports = login;