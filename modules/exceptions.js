module.exports = {
  INVALID_ROLE: "Please provide a valid role.",
  INVALID_EMAIL: "Please provide a valid email address.",
  UNABLE_TO_SAVE: "Unable to save record. Please contact your administrator.",
  RECORD_EXISTS: "A [ROLE] record already exists for this email address.",
  INVALID_GOAL: "Please provide a valid goal",
  INVALID_LOGIN: "Please provide a valid email and password",
};
