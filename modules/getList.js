function getList(role){
	"use strict";
	const fs = require("fs");
	const path = require("path");
	let list = [];
	let peopleData = fs.readFileSync(path.resolve(__dirname, "../json/person.json"));
	let people = JSON.parse(peopleData);
	if(role){ // only allow lists based on role
		people.forEach((person)=>{
			if(person.Role === role){
				list.push(person);
			} else if(Array.isArray(person.Role)){
				let isRole = false;
				person.Role.forEach((role1)=>{
					if(role1===role){
						isRole = true;
					}
				});
				if(isRole){
					list.push(person);
				}
			}
		});
	}
	return list;
}

module.exports = getList;
