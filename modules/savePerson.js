/**
 * creates a new person record based on the passed in parameters
 * person = {Id: per-0001, FirstName: Test, LastName: Mentor, Email: test.mentor@perficient.com, Role: Mentor }
 *
 * @param {*} firstName
 * @param {*} lastName
 * @param {*} email
 * @param {*} password 
 * @param {*} role - acceptable values are Mentor, Mentee, and Program Director
 * @returns new Person record
 */

function savePerson(firstName, lastName, email, password, role) {
	"use strict";
	const fs = require("fs");
	const path = require("path");
	const exceptions = require("./exceptions");

	if (!email) {
		throw exceptions.INVALID_EMAIL;
	}

	if(!role || (Array.isArray(role) && !role.length)){
		throw exceptions.INVALID_ROLE;
	} else if(typeof role==="string"){
		role = [role];
	}

	let rawData = fs.readFileSync(path.resolve(__dirname, "../json/person.json"));
	let people = JSON.parse(rawData);

	let exists = false;
	people.forEach((person) => {
		if (person.Email === email) {
			exists = true;
		}
	});

	if (!exists) {
		// we add a new mentor record		
		let person = {
			FirstName: firstName,
			LastName: lastName,
			Email: email,
			Password: password,
			Role: role,
		};
		people.push(person);
		fs.writeFileSync(
			path.resolve(__dirname, "../json/person.json"),
			JSON.stringify(people, null, 2)
		);
		return person;
	} else {
		// person already exists
		throw exceptions.RECORD_EXISTS.replace("[ROLE]", role);
		//	return result[0];
	}
}

module.exports = savePerson
