const saveGoals = require("../../modules/saveGoals");
const exceptions = require("../../modules/exceptions");

let email = "test123@perficient.com";
let goal = "I want to learn more";
let timeframe = "long";
let goalObect = [{ goal, timeframe }];

test("should create a new goal", () => {
  let result = saveGoals(goal, timeframe, email);
  expect(result.Goals).toStrictEqual(goalObect);
});

test("should throw an error for wmpty goal and timeframe", () => {
  goal = "";
  timeframe = "";
  try {
    saveGoals(goal, timeframe, email);
    expect(error).toBe(exceptions.INVALID_GOAL);
  } catch (error) {
    expect(error).toBe(exceptions.INVALID_GOAL);
  }
});

test("should throw an error", () => {
  email = "test123weqwe@perficient.com";
  goal = "I want to learn more";
  timeframe = "long";
  try {
    saveGoals(goal, timeframe, email);
    expect(true).toBe(false); // this test should produce an error
  } catch (error) {
    expect(error).toBe(exceptions.UNABLE_TO_SAVE);
  }
});

afterEach(() => {
  clearTestData();
});

function clearTestData() {
  const fs = require("fs");
  const path = require("path");
  let rawData = fs.readFileSync(
    path.resolve(__dirname, "../../json/person.json")
  );
  let people = JSON.parse(rawData);
  people.forEach((person) => {
    if (person.Email === email) {
      person.Goals = [];
    }
  });
  fs.writeFileSync(
    path.resolve(__dirname, "../../json/person.json"),
    JSON.stringify(people, null, 2)
  );
}
