const savePerson = require('../../modules/savePerson');
const exceptions = require('../../modules/exceptions');

let email = 'jest.test@perficient.com';
let firstName = 'Jest';
let lastName = 'Test';
let role = ['Mentor'];
let password = "12345";

test('should create a new person with role of mentor', () => {
    let result = savePerson(firstName, lastName, email, password, role);
	console.log(result);
    expect(result.Email).toBe(email);
    expect(result.FirstName).toBe(firstName);
    expect(result.LastName).toBe(lastName);
	expect(result.Role).toBe(role);
	expect(result.Password).toBe(password); // TODO :: don't store the password -- encrypt it
});

test('should throw an error that a record already exists', () => {
    let result = savePerson(firstName, lastName, email, password, role);
    try {
        let result2 = savePerson(firstName, lastName, email, password, role);
        expect(true).toBe(false); // this test should produce an error
    } catch (error) {
        expect(error).toBe(exceptions.RECORD_EXISTS.replace("[ROLE]", role));
    }
});

test('should throw an error that the email address is missing', () => {
    try {
        let result = savePerson(firstName, lastName, null, password, role);
        expect(true).toBe(false); // this test should produce an error
    } catch (error) {
        expect(error).toBe(exceptions.INVALID_EMAIL);
    }
});

test('should throw an error that the role is missing', () => {
    try {
        let result = savePerson(firstName, lastName, email, password, null);
        expect(true).toBe(false); // this test should produce an error
    } catch (error) {
        expect(error).toBe(exceptions.INVALID_ROLE);
    }
});

afterEach(() => {
    clearTestData();
});

function clearTestData() {
    const fs = require('fs');
    const path = require('path');
    let rawData = fs.readFileSync(path.resolve(__dirname, '../../json/person.json'));
    let people = JSON.parse(rawData);
    let updatedPeople = [];
    people.forEach((person) => {
        if (person.Email !== email) {
            updatedPeople.push(person);
        }
    });
    fs.writeFileSync(
        path.resolve(__dirname, '../../json/person.json'),
        JSON.stringify(updatedPeople, null, 2)
    );
}