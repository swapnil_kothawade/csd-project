const login = require('../../modules/login');
const savePerson = require('../../modules/savePerson');
const exceptions = require('../../modules/exceptions');

let email = 'jest.test@perficient.com';
let firstName = 'Jest';
let lastName = 'Test';
let role = ['Mentor'];
let password = "12345";

beforeEach(()=>{
	savePerson(firstName, lastName, email, password, role);
});

test('should return success with correct password', () => {
    let result = login(email, password);
    expect(result.Email).toBe(email);
	expect(result.Password).toBe(password); // TODO :: don't store the password -- encrypt it
});

test('should throw an exception for incorrect password', () => {
	try {
		let result = login(email, 'bogus');
		expect(true).toBe(false); // this login attempt should fail
	} catch(e){
		expect(e).toBe(exceptions.INVALID_LOGIN);
	}
});

afterEach(()=>{
	clearTestData();
});

function clearTestData() {
    const fs = require('fs');
    const path = require('path');
    let rawData = fs.readFileSync(path.resolve(__dirname, '../../json/person.json'));
    let people = JSON.parse(rawData);
    let updatedPeople = [];
    people.forEach((person) => {
        if(person.Email !== email) {
            updatedPeople.push(person);
        }
    });
    fs.writeFileSync(
        path.resolve(__dirname, '../../json/person.json'),
        JSON.stringify(updatedPeople, null, 2)
    );
}