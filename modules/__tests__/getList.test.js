const getList = require('../../modules/getList');
const savePerson = require('../../modules/savePerson');

let mentor = {
	Email : 'jest.test.mentor@perficient.com',
	FirstName : 'JestTest',
	LastName : 'Mentor',
	Password : '12345',
	Role : ['Mentor']
};

let mentee = {
	Email : 'jest.test.mentee@perficient.com',
	FirstName : 'JestTest',
	LastName : 'Mentee',
	Password : '12345',
	Role : ['Mentee']
};

beforeEach(()=>{
	try {
		savePerson(mentor.FirstName, mentor.LastName, mentor.Email, mentor.Password, mentor.Role);
	} catch(e){
		// mentor already exists
	}
	try {
		savePerson(mentee.FirstName, mentee.LastName, mentee.Email, mentee.Password, mentee.Role);
	} catch(e){
		// mentee already exists
	}
});

test('should return a list of mentors with at least one record', () => {
    let result = getList('Mentor');
    expect(result.length).toBeGreaterThan(0);
});

test('should return a list of mentees with at least one record', () => {
	let result = getList('Mentee');
	expect(result.length).toBeGreaterThan(0);
});

test('should return an empty list because no role is defined', () => {
	let result = getList(null);
	expect(result.length).toBe(0);
});

afterEach(()=>{
	clearTestData();
});

function clearTestData() {
    const fs = require('fs');
    const path = require('path');
    let rawData = fs.readFileSync(path.resolve(__dirname, '../../json/person.json'));
    let people = JSON.parse(rawData);
    let updatedPeople = [];
    people.forEach((person) => {
        if(person.Email !== mentor.Email && person.Email !== mentee.Email) {
            updatedPeople.push(person);
        }
    });
    fs.writeFileSync(
        path.resolve(__dirname, '../../json/person.json'),
        JSON.stringify(updatedPeople, null, 2)
    );
}