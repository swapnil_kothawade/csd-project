const updateMentor = require('../../modules/updateMentor');
const savePerson = require('../../modules/savePerson');
const exceptions = require('../../modules/exceptions');

let mentorEmail = 'jest.test@perficient.com';
let menteeEmail = 'joker.test@perficient.com';

test('should update a new mentee', () => {
    let saveResult = savePerson("John", "Tester", mentorEmail, "12345", ["Mentor"]);
    let result = updateMentor(mentorEmail, menteeEmail);
    expect(result.Email).toBe(mentorEmail);
    expect(result.MenteeEmail).toBe(menteeEmail);
});

test('should throw an error if cannot be found', () => {
    try {
        let result = updateMentor(mentorEmail, menteeEmail);
        expect(true).toBe(false); // this test should produce an error
    } catch (error) {
        expect(error).toBe(exceptions.UNABLE_TO_SAVE);
    }
});


afterEach(() => {
    clearTestData();
});

function clearTestData() {
    const fs = require('fs');
    const path = require('path');
    let rawData = fs.readFileSync(path.resolve(__dirname, '../../json/person.json'));
    let people = JSON.parse(rawData);
    let updatedPeople = [];
    people.forEach((person) => {
        if (person.Email !== mentorEmail && person.Email !== menteeEmail ) {            
            updatedPeople.push(person);
        }
    });
    fs.writeFileSync(
        path.resolve(__dirname, '../../json/person.json'),
        JSON.stringify(updatedPeople, null, 2)
    );
}