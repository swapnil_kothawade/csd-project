/**
 * creates a new person record based on the passed in parameters
 * person = {Id: per-0001, FirstName: Test, LastName: Mentor, Email: test.mentor@perficient.com, Role: Mentor }
 *
 * @param {*} goal - mentee listed goal
 * @param {*} timeframe - timeframe for goal
 * @returns the person that gets updated
 */

function saveGoals(goal, timeframe, email) {
  "use strict";
  const fs = require("fs");
  const path = require("path");
  const exceptions = require("./exceptions");

  if (!goal || !timeframe) {
    throw exceptions.INVALID_GOAL;
  }

  let rawData = fs.readFileSync(path.resolve(__dirname, "../json/person.json"));
  let people = JSON.parse(rawData);

  let updated = false;
  let personToReturn;

  people.forEach((person) => {
    if (person.Email === email) {
      updated = true;
      let goalObject = { goal, timeframe };
      if (!person.Goals || !Array.isArray(person.Goals)) {
        person.Goals = [];
      }
      person.Goals.push(goalObject);
      personToReturn = person;
    }
  });

  if (updated) {
    fs.writeFileSync(
      path.resolve(__dirname, "../json/person.json"),
      JSON.stringify(people, null, 2)
    );
    return personToReturn;
  } else {
    // throw an exception
    throw exceptions.UNABLE_TO_SAVE;
  }
}

module.exports = saveGoals;
