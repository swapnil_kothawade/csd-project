/**
 * creates a new person record based on the passed in parameters
 * person = {Id: per-0001, FirstName: Test, LastName: Mentor, Email: test.mentor@perficient.com, Role: Mentor }
 *
 * @param {*} mentorEmail - email format
 * @param {*} menteeEmail - email format
 * @returns the person that gets updated
 */

 function updateMentor(mentorEmail, menteeEmail) {
	"use strict";
	const fs = require("fs");
	const path = require("path");
	const exceptions = require("./exceptions");

	if (!menteeEmail || !mentorEmail) {
		throw exceptions.INVALID_EMAIL;
	}


	let rawData = fs.readFileSync(path.resolve(__dirname, "../json/person.json"));
	let people = JSON.parse(rawData);


	let updated = false;
    let personToReturn;
	people.forEach((person) => {
		if (person.Email === mentorEmail) {
            updated = true;
		    person["MenteeEmail"]= menteeEmail;
            personToReturn = person;
		}
	});

	if (updated) {
		fs.writeFileSync(
			path.resolve(__dirname, "../json/person.json"),
			JSON.stringify(people, null, 2)
		);
		return personToReturn;
	} else {
		// throw an exception
		throw exceptions.UNABLE_TO_SAVE;
	}
}

module.exports = updateMentor
